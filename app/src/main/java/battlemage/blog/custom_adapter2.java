package battlemage.blog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by battlemage on 10/3/2017.
 */

public class custom_adapter2 extends BaseAdapter{

    String Username[];
    String Title[];

    LayoutInflater inflater;

    public custom_adapter2(Context context,String[] Username, String[] Title){
        this.Username = Username;
        this.Title = Title;
        inflater = (LayoutInflater.from(context));
    }
    @Override
    public int getCount() {
        return Title.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.custom_list,viewGroup,false);
        TextView CustomUsername = (TextView)view.findViewById(R.id.txtUsername);
        TextView CustomTitle = (TextView)view.findViewById(R.id.txtTitle);

        CustomUsername.setText(Username[i]);
        CustomTitle.setText(Title[i]);
        return view;
    }
}
