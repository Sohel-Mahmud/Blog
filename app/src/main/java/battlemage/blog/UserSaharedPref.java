package battlemage.blog;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by battlemage on 10/3/2017.
 */

public class UserSaharedPref {
    private String id, username;
    private boolean loggedIn;
    private Context context;
    private SharedPreferences sp;

    public UserSaharedPref(Context context){
        this.context=context;
        sp=context.getSharedPreferences("completePref",Context.MODE_PRIVATE);
    }

    public void setId(String id) {
        SharedPreferences.Editor editor= sp.edit();
        editor.putString("userId",id);
        editor.apply();
    }

    public void setUsername(String username) {
        SharedPreferences.Editor editor= sp.edit();
        editor.putString("username",username);
        editor.apply();
    }

    public String getId() {
        return sp.getString("userId","");
    }

    public String getUsername() {
        return sp.getString("username","");
    }

    public boolean isLoggedIn(){return sp.getBoolean("logged",false);}

    public void setLoggedIn(boolean loggedIn){
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("logged",loggedIn);
        editor.apply();
    }
    public void clearAll(){
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
    }
}
