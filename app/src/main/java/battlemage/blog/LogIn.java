package battlemage.blog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LogIn extends AppCompatActivity {

    private ProgressDialog pd;
    private EditText edtEmail,edtPassword;
    private Button btnLogin,btnRegister;
    private RequestQueue rq;
    private UserSaharedPref usp;
    //private String onlineURL="http://10.0.3.2/Blog/login.php";
    private String onlineURL="https://battlemage.000webhostapp.com/blog/login.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        edtEmail=(EditText) findViewById(R.id.edt_login_email);
        edtPassword=(EditText) findViewById(R.id.edt_login_password);
        pd=new ProgressDialog(this);
        pd.setTitle("loading...");
        pd.setMessage("Please Wait");

        usp = new UserSaharedPref(LogIn.this);

        btnLogin=(Button) findViewById(R.id.btn_login_login);
        btnRegister=(Button) findViewById(R.id.btn_login_register);
        rq= Volley.newRequestQueue(this);
        checkLogged();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogIn.this,Registration.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String strEmail,strPassword;
                strEmail = edtEmail.getText().toString().trim();
                strPassword = edtPassword.getText().toString().trim();

                if(TextUtils.isEmpty(strEmail) && TextUtils.isEmpty(strPassword)){
                    Toast.makeText(LogIn.this, "Fill all the fields", Toast.LENGTH_SHORT).show();
                }
                else{
                    pd.show();
                    sendDataToServer(strEmail,strPassword);

                }
            }
        });
    }

    private void sendDataToServer(final String strEmail, final String strPassword) {
        StringRequest saveDataRequest = new StringRequest(Request.Method.POST, onlineURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("serverResponse",response);
                try{
                    JSONObject userData = new JSONObject(response);

                    usp.setLoggedIn(true);
                    usp.setId(userData.getString("id"));
                    usp.setUsername(userData.getString("username"));
                    LogIn.this.finish();
                    startActivity(new Intent(LogIn.this,Home.class));
                }catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(LogIn.this, response, Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("serverError", error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("email",strEmail);
                params.put("password",strPassword);
                return params;
            }
        };
        rq.add(saveDataRequest);
    }
    private void checkLogged(){
        if(usp.isLoggedIn()){
            LogIn.this.finish();
            startActivity(new Intent(LogIn.this,Home.class));
        }
    }
}
