package battlemage.blog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by battlemage on 10/3/2017.
 */

public class Comment_adapter extends BaseAdapter{

    String Username[];
    String Comment[];

    LayoutInflater inflater;

    public Comment_adapter(Context context,String[] Username, String[] Comment){
        this.Username = Username;
        this.Comment = Comment;
        inflater = (LayoutInflater.from(context));
    }
    @Override
    public int getCount() {
        return Username.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.custom_comment_list,viewGroup,false);
        TextView CustomUsername = (TextView)view.findViewById(R.id.txtCommentUsername);
        TextView CustomDetail = (TextView)view.findViewById(R.id.txtCommentDetail);

        CustomUsername.setText(Username[i]);
        CustomDetail.setText(Comment[i]);
        return view;
    }
}
