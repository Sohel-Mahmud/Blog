package battlemage.blog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Home extends AppCompatActivity {

    UserSaharedPref usp;
    ListView datalist;
    custom_adapter customAdapter;
    String[] post_id, user_id, Username, Title, description;
    //private String URL = "http://10.0.3.2/Blog/getdata.php";
    private String URL = "https://battlemage.000webhostapp.com/blog/getdata.php";
    RequestQueue req;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        usp = new UserSaharedPref(this);
        datalist = (ListView)findViewById(R.id.txtList);
        req = Volley.newRequestQueue(this);
        fetchData();

        datalist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in = new Intent(Home.this, ViewPost.class);
                in.putExtra("post_id",post_id[i]);
                in.putExtra("username",Username[i]);
                in.putExtra("title",Title[i]);
                in.putExtra("description",description[i]);
                startActivity(in);
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,UploadPost.class));

            }
        });
    }

    private void fetchData() {
        StringRequest getAllData = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("serverData",response);
                try{
                    JSONArray TotalData = new JSONArray(response);
                    post_id = new String[TotalData.length()];
                    user_id = new String[TotalData.length()];
                    Username = new String[TotalData.length()];
                    Title = new String[TotalData.length()];
                    description = new String[TotalData.length()];

                    customAdapter = new custom_adapter(getApplicationContext(),Username,Title);

                    for(int i=0; i<TotalData.length(); i++){
                        JSONObject singleData = TotalData.getJSONObject(i);
                        post_id[i] = singleData.getString("post_id");
                        user_id[i] = singleData.getString("user_id");
                        Username[i] = singleData.getString("username");
                        Title[i] = singleData.getString("title");
                        description[i] = singleData.getString("description");

                    }
                }catch (JSONException e){
                    e.printStackTrace();

                }
                datalist.setAdapter(customAdapter);
                //refreshig data after update
                customAdapter.notifyDataSetChanged(); //also need to override fetchdata onStart();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        req.add(getAllData);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fetchData();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Home.this,Home.class);
            startActivity(intent);
            return true;
        }else if(id==R.id.logout){
            usp.clearAll();
            Home.this.finish();
            startActivity(new Intent(Home.this,LogIn.class));
        }else if(id==R.id.mypost){
            //String sharedprefid;
            //sharedprefid = usp.getId();
            //Toast.makeText(this, sharedprefid, Toast.LENGTH_SHORT).show();
            Intent in = new Intent(Home.this,MyPosts.class);
            //in.putExtra("id",sharedprefid);
            startActivity(in);
        }
        else if(id==R.id.exit){
            new android.app.AlertDialog.Builder(Home.this)
                    .setTitle("Exit Confirmation")
                    .setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}

