package battlemage.blog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyPosts extends AppCompatActivity {

    UserSaharedPref usp;
    ListView Postdatalist;
    String LoggedUserId;
    custom_adapter2 customPostAdapter;
    String[] post_id, user_id, Username, Title, description;
    //private String URL = "http://10.0.3.2/Blog/loggeduserdata.php";
    private String URL = "https://battlemage.000webhostapp.com/blog/loggeduserdata.php";
    //private String deleteURL = "http://10.0.3.2/Blog/deleteuserdata.php";
    private String deleteURL = "https://battlemage.000webhostapp.com/blog/deleteuserdata.php";
    RequestQueue que;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        usp = new UserSaharedPref(this);
        Postdatalist = (ListView)findViewById(R.id.txtMyPostList);
        que = Volley.newRequestQueue(this);

        LoggedUserId=usp.getId();
        fetchMyPostData(LoggedUserId);

        Postdatalist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in = new Intent(MyPosts.this, ViewPost.class);
                in.putExtra("post_id",post_id[i]);
                in.putExtra("username",Username[i]);
                in.putExtra("title",Title[i]);
                in.putExtra("description",description[i]);
                startActivity(in);
            }
        });
        Postdatalist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                AlertDialog.Builder confirmDelete = new AlertDialog.Builder(MyPosts.this);

                confirmDelete.setTitle("Do u want to delete?");
                confirmDelete.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendToServer(post_id[position]);
                        dialogInterface.dismiss();
                        //for notifying after delete
                        customPostAdapter.notifyDataSetChanged();
                        //fetching datas agsain to show
                        fetchMyPostData(LoggedUserId);
                    }
                });
                confirmDelete.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                confirmDelete.show();
                return true;
            }
        });

    }

    private void sendToServer(final String s) {
        StringRequest deleteData = new StringRequest(Request.Method.POST, deleteURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MyPosts.this,response, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String ,String> map = new HashMap<>();
                map.put("post_id",s);
                return map;
            }
        };
        que.add(deleteData);
    }

    private void fetchMyPostData(final String LoggedUserId) {
        StringRequest getUserPost = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("userPost",response);
               try{
                    JSONArray TotalUserData = new JSONArray(response);
                    post_id = new String[TotalUserData.length()];
                    user_id = new String[TotalUserData.length()];
                    Username = new String[TotalUserData.length()];
                    Title = new String[TotalUserData.length()];
                    description = new String[TotalUserData.length()];

                    customPostAdapter = new custom_adapter2(getApplicationContext(),Username,Title);

                    for(int i=0; i<TotalUserData.length(); i++){
                        JSONObject singleData = TotalUserData.getJSONObject(i);
                        post_id[i] = singleData.getString("post_id");
                        user_id[i] = singleData.getString("user_id");
                        Username[i] = singleData.getString("username");
                        Title[i] = singleData.getString("title");
                        description[i] = singleData.getString("description");

                    }
                }catch (JSONException e){
                    e.printStackTrace();

                }
                Postdatalist.setAdapter(customPostAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("user_id",LoggedUserId);
                return map;
            }
        };
        que.add(getUserPost);
    }
    @Override
    protected void onStart() {
        super.onStart();
        fetchMyPostData(LoggedUserId);
    }

}
