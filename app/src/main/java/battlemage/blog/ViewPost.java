package battlemage.blog;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ViewPost extends AppCompatActivity {

    String postID,username,title,description;
    TextView txtPostUsername,txtPosTitle, txtPostDesc;
    ListView commentlist;
    String[] post_com_id, Username,Comment;
    Comment_adapter commentAdapter;
    RequestQueue rq;
    //private String commentURL="http://10.0.3.2/Blog/commentlist.php";
    private String commentURL="https://battlemage.000webhostapp.com/blog/commentlist.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        rq = Volley.newRequestQueue(this);

        postID = getIntent().getExtras().getString("post_id");
        username = getIntent().getExtras().getString("username");
        title = getIntent().getExtras().getString("title");
        description = getIntent().getExtras().getString("description");

        txtPostUsername = (TextView)findViewById(R.id.txtPostUsername);
        txtPosTitle = (TextView)findViewById(R.id.txtPostTitle);
        txtPostDesc = (TextView)findViewById(R.id.txtPostDetail);
        commentlist = (ListView)findViewById(R.id.CommentList);

        txtPostUsername.setText("User: "+username);
        txtPosTitle.setText(title);
        txtPostDesc.setText(description);
        fetchComment(postID);

    }

    private void fetchComment(final String postID) {
        StringRequest getComment = new StringRequest(Request.Method.POST, commentURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("PostComment",response);
                try{
                    JSONArray TotalUserData = new JSONArray(response);
                    post_com_id = new String[TotalUserData.length()];
                    Username = new String[TotalUserData.length()];
                    Comment = new String[TotalUserData.length()];

                    commentAdapter = new Comment_adapter(getApplicationContext(),Username,Comment);

                    for(int i=0; i<TotalUserData.length(); i++){
                        JSONObject singleData = TotalUserData.getJSONObject(i);
                        post_com_id[i] = singleData.getString("post_com_id");
                        Username[i] = singleData.getString("username");
                        Comment[i] = singleData.getString("comment");

                    }
                }catch (JSONException e){
                    e.printStackTrace();

                }
                commentlist.setAdapter(commentAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("post_com_id",postID);
                return map;
            }
        };
        rq.add(getComment);
    }
}
