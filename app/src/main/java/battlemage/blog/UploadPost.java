package battlemage.blog;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UploadPost extends AppCompatActivity {
    private ProgressDialog pd;
    private EditText edtTitle,edtDesc;
    private Button btnUpload;
    private RequestQueue rq;
    UserSaharedPref usf;
    //private String URL="http://10.0.3.2/Blog/uploaddata.php";
    private String URL="https://battlemage.000webhostapp.com/blog/uploaddata.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_post);

        pd=new ProgressDialog(this);
        pd.setTitle("loading...");
        pd.setMessage("Please Wait");

        usf = new UserSaharedPref(this);

        edtTitle=(EditText) findViewById(R.id.edt_task_title);
        edtDesc=(EditText) findViewById(R.id.edt_task_desc);

        btnUpload=(Button) findViewById(R.id.btn_task_upload);

        rq= Volley.newRequestQueue(this);

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strID,strUsername,strTitle,strDesc;
                strID = usf.getId();
                strUsername = usf.getUsername();
                strTitle=edtTitle.getText().toString().trim();
                strDesc=edtDesc.getText().toString();
                sendToServer(strID,strUsername,strTitle,strDesc);
            }
        });
    }

    //sending data to server using StringRequest POST method using map for key value
    private void sendToServer(final String strID,final String strUsername,final String strTitle, final String strDesc) {
        //request queu
        StringRequest uploadData = new StringRequest(Request.Method.POST,URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response){
                Toast.makeText(UploadPost.this, response, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("user_id",strID);
                map.put("username",strUsername);
                map.put("title",strTitle);
                map.put("description",strDesc);
                return map;
            }
        };
        //send data to req queu
        rq.add(uploadData);

    }
}
